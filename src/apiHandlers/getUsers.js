import axios from "axios";
import {serverBus} from "../globalEvtBus";


export default function () {
    return axios.get("user").then(resp => {
        serverBus.$emit("userList", resp.data);
        return resp.data;
    }).catch(() => {
        return [{}]
    });
}