import axios from 'axios';
import {serverBus} from "./globalEvtBus";
import UserInput from './components/userInput/UserInput.vue';
import UserData from './components/userData/UserData';
import getUsers from "./apiHandlers/getUsers";


export default {
    name: 'app',
    data: function() {
        return {
            userFields: ["Username", "Surname", "Job"],
            value: "some!!!",
            dataToSend: {},
            listOfUsers: [{}]
        }
    },
    components: {
        UserInput, UserData
    },
    created() {
        serverBus.$on('Username', (val) => {
            this.dataToSend['username'] = val;
        });
        serverBus.$on('Surname', (val) => {
            this.dataToSend['surname'] = val;
        });
        serverBus.$on('Job', (val) => {
            this.dataToSend['job'] = val;
        })
    },
    mounted () {
        getUsers().then(resp => {
            this.listOfUsers = resp;
        })
    },
    methods: {
        sendData() {
            axios.post("user", this.dataToSend, {
                headers: {
                    "Content-type": "application/json"
                }
            }).catch(function (error) {
                alert("there was an error: " + error)
            });
        },
        updateUsersList() {
            getUsers().then(resp => {
                this.listOfUsers = resp;
            })
        }
    }
}