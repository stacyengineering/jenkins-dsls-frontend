import {serverBus} from "../../globalEvtBus";


export default {
    name: "UserInput",
    props: ['inputName', 'inputValue'],
    methods: {
        handleChange() {
            serverBus.$emit(this.inputName, this.inputValue);
        }
    },
    computed: {
    //    todo: validate all fields are inputted!
    }
}